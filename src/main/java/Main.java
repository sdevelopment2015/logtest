public class Main {

   public static void main(String[] args) {
        //Call 3 threads
       Thread thread1 = new Thread(new DemoThread("Some text1"));
       Thread thread2 = new Thread(new DemoThread("Some other text2"));
       Thread thread3 = new Thread(new DemoThread("Some strange text3"));
       thread1.start();
       thread2.start();
       thread3.start();
    }
}
