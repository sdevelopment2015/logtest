/**
 * Class is created for work with multithreads. It call method log() of Logger.class
 */
public class DemoThread  extends Thread{

    private Thread thread;
    private String text;
    private Logger logger = new Logger();

    //String parameter was set in constructor for input the text in method log() that is called
    //automatically by calling method start()
    public DemoThread(String text){
        this.text = text;
    }

    public void run(){
        logger.log(text);
    }

    public void start(){
        if(thread == null){
            thread = new Thread(new DemoThread(text));
            thread.start();
        }
    }

}
