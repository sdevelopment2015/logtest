import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Class make logging by its single public method log()
 */
public class Logger {

    private LocalDateTime localDateTime;
    private String threadName;
    private String aClass;
    private String text;
    private static final String newLine = System.getProperty("line.separator");

    private final String fileDestination = "src/main/resources/loggs/fileA.txt";
    private final String htmlFile = "src/main/resources/loggs/fileA.html";

    /**
     * Method writes logs in console, in .txt file and open in browser a .html version of file.
     * @param text - given text for log
     */
    public void log(String text) {
        //take name of current thread
        threadName = Thread.currentThread().getName();
        //identify what class call method log()
        aClass = Thread.currentThread().getStackTrace()[2].getClassName();
        //take current date and time
        localDateTime = LocalDateTime.now();
        //code below works only if text is different
        if (this.text == null || !this.text.equals(text)) {
            this.text = text;
            String fullText = String.format("%s %s %s: %s %s: %s", localDateTime, ": Thread name: ",
                    threadName, "Class: ", aClass, text);
            writeConsole(fullText);
            writeToDoc(fullText + newLine); // + newLine - add next text from new line in file
            writeNetwork(fullText);
        }
    }

    /**
     * Method is marked as synchronized for it will be free for one thread only at the same time
     * @param text - text for writing
     */
    private synchronized void writeToDoc(String text) {
        Path filePath = Paths.get(fileDestination);
        try {
            //if file is not exist create the new one
            if (!Files.exists(filePath)) {
                File newFile = new File(fileDestination);
                if (newFile.createNewFile()) {
                    System.out.println("New file created");
                }
            }
            //update file, write in existing text
            Files.write(filePath, text.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println("ERROR in writing logs to file!");
        }
    }

    /**
     * Method writes text in console. Synchronized for all text be in order: one thread after other
     * @param text - text for write
     */
    private synchronized void writeConsole(String text) {
        System.out.println(text);
    }

    /**
     * Method displays text in a browser as html. Method is synchronized for write and read from file by one thread
     * @param text
     */
    private synchronized void writeNetwork(String text) {

        boolean success = CompileToWebPage(fileDestination, htmlFile);
        if (success) {
            System.out.println("Text File Successfully Compiled!");
        } else {
            System.out.println("Text File Compilation FAILED!");
        }

        //Display our new file in the web Browser...
        try {
            File htmlFiledisplay = new File(htmlFile);
            Desktop.getDesktop().browse(htmlFiledisplay.toURI());
        } catch (IOException ex) {
        }
    }

    /**
     * This method convert .txt file to .html
     * @param sourcefilePath - url of .txt file
     * @param destinationFilePath - url of .html file
     * @return true if conversion is successful and false if conversion was fault.
     */
    private boolean CompileToWebPage(String sourcefilePath, String destinationFilePath) {

        BufferedReader input;
        try {
            input = new BufferedReader(new FileReader(sourcefilePath));
            if (!input.ready()) {
                throw new IOException();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File does not exist");
            return false;
        } catch (IOException ex) {
            System.out.println("ERROR in reading txt file");
            return false;
        }

        // Place required HTML Tags into String ArrayList
        ArrayList<String> txt = new ArrayList<>();
        txt.add("<html>");
        txt.add("<body>");

        //Read lines and add tags
        try {
            String str;
            while ((str = input.readLine()) != null) {
                txt.add("<p>" + str + "</p>");
            }
            input.close();
        } catch (IOException ex) {
            System.out.println("ERROR with reading lines of text");
            return false;
        }

        //Add html tags
        txt.add("</body>");
        txt.add("</html>");

        //Write html to a new file
        try {
            FileWriter fw = new FileWriter(destinationFilePath);
            Writer output = new BufferedWriter(fw);

            for (int i = 0; i < txt.size(); i++) {
                output.write(txt.get(i) + "\r\n");
            }
            output.close();
            return true;
        } catch (IOException ex) {
            System.out.println("ERROR with reading html file");
            return false;
        }
    }
}
